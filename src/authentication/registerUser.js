import axiosInstance from '../axios';

export function RegisterUser(formData, successCallback, errorCallback, redirect) {
    axiosInstance
    .post(`auth/signup`, {
        username: formData.username,
		email: formData.email,
		password: formData.password,
        password2: formData.password2,
        roles: ["user"]
    })
    .then(() => {
        successCallback(formData, redirect, errorCallback);
    })
    .catch((err) => {
        let errors ={};
        errors["404"] = "Error credential: " + err;
        errorCallback(errors);
    });
}