import axiosInstance from '../axios';

export function LoginUser(formData, successCallback, errorCallback) {
    axiosInstance
    .post(`auth/signin`, {
        email: formData.email,
		password: formData.password,
    })
    .then((res) => {				
        localStorage.setItem('access_token', res.data.accessToken);
		axiosInstance.defaults.headers['x-access-token'] = localStorage.getItem('access_token');
        successCallback(res.data.accessToken);
    })
    .catch((err) => {
        let errors ={};
        errors["404"] = "Email not registered: " + err;
        errorCallback(errors);
    });
}