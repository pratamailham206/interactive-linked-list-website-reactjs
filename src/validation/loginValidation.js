export function loginValidation(formData, callback) {
    let formIsValid = true;
    let errors = {};
    if (formData.email === "") {
        formIsValid = false;
        errors["email"] = "email can't be empty";
    }
    if (formData.password === "") {
        formIsValid = false;
        errors["password"] = "password can't be empty";
    }
    if (!formIsValid) {
        callback(errors);
    }

    return formIsValid;
};