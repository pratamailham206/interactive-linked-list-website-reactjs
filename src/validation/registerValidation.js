export function RegisterValidation(formData, callback) {
    let formIsValid = true;
    let errors = {};
    if (formData.username === "") {
        formIsValid = false;
        errors["username"] = "Username can't be empty";
    }
    if (formData.email === "") {
        formIsValid = false;
        errors["email"] = "email can't be empty";
    }
    if (formData.password === "") {
        formIsValid = false;
        errors["password"] = "password can't be empty";
    }
    if (formData.password2 === "") {
        formIsValid = false;
        errors["password2"] = "password2 can't be empty";
    }
    if (formData.password !== formData.password2) {
        formIsValid = false;
        errors["match"] = "Password not match";
    }

    if (!formIsValid) {
        callback(errors);
    }

    return formIsValid
};