import React, { useCallback, useEffect, useRef } from 'react';
import { useState } from 'react/cjs/react.development';
import { useParams } from 'react-router-dom';
import SingleLinkCodeGenerator from '../../code/SingleLinkCodeGenerator';
import DoubleLinkCodeGenerator from '../../code/DoubleLinkCodeGenerator';
import CircularLinkCodeGenerator from '../../code/CircularLinkCodeGenerator';
import CodeEditor from './Code/codeEditor';
import ToolEditor from './Tools/toolEditor';
import NodeEditor from './Nodes/nodeEditor';
import axiosInstance from '../../axios';

export default function Editor() {
    let { listid } = useParams();
    const [ codeId, setCodeId ] = useState(0);

    const [ structData, setStructData ] = useState([{}]);
    const [ structName, setStructName ] = useState("");
    const [ projectType, setProjectType ] = useState('');

    const [ code, setCode] = useState(``);
    const [ valueData, setValueData ] = useState([]);

    const [ operations, setOperations ] = useState([]);
    const [ operationLogs, setOperationLogs] = useState([]);

    const nodeRef = useRef();

    const CreateCodeInitialData = useCallback((id) => {
        axiosInstance.post('/log/create', {
            data: operationLogs,
            codeId: id
        }).then((res) => {
            console.log(res);
        }).catch((err) => {
            console.log(err);
        });
        axiosInstance.post('/operation/create', {
            data: operations,
            codeId: id
        }).then((res) => {
            console.log(res);
        }).catch((err) => {
            console.log(err);
        });
    }, [operationLogs, operations]);

    const CreateNewNodeData = useCallback(() => {
        axiosInstance.post('/node/create', {
            data: valueData,
            listId: listid
        }).then((res) => {
            console.log(res);
        }).catch((err) => {
            console.log(err);
        });
    }, [valueData, listid]);

    const CreateNewCodeData = useCallback(() => {
        axiosInstance.post('/code/create', {
            data: code,
            listId: listid
        }).then((res) => {
            setCodeId(res.data.data.id);
            CreateCodeInitialData(res.data.data.id);
        }).catch((err) => {
            console.log(err);
        });
    }, [CreateCodeInitialData, listid, code, setCodeId]);

    const GetCodeData = useCallback(() => {
        axiosInstance.get(`/code/detail/${listid}`)
        .then((res) => {
            console.log(res.data);
            setCodeId(res.data.id);
            setCode(res.data.data);
            setOperations(res.data.operation.data);
            setOperationLogs(res.data.log.data);
        })
        .catch((err) => {
            console.log(err);
        })
    }, [listid, setCode, setOperations, setOperationLogs, setCodeId]);

    const GetNodeData = useCallback(() => {
        axiosInstance.get(`/node/detail/${listid}`)
        .then((res) => {
            setValueData(res.data.data);
        })
        .catch((err) => {
            console.log(err);
        })
    }, [listid, setValueData]);

    function UpdateCodeData(_code, _log, _operation) {
        axiosInstance.put('/code/update', {
            data: _code,
            listId: listid
        }).then((res)=> {
            console.log(res);
        }).catch((err) => {
            console.log(err);
        });
        axiosInstance.put('/log/update', {
            data: _log,
            codeId: codeId
        }).then((res)=> {
            console.log(res);
        }).catch((err) => {
            console.log(err);
        });
        axiosInstance.put('/operation/update', {
            data: _operation,
            codeId: codeId
        }).then((res)=> {
            console.log(res);
        }).catch((err) => {
            console.log(err);
        });
    }

    const getInitialStructData = useCallback(() => { 
        axiosInstance
        .get(`/list/detail/${listid}`)
        .then((res) => {
            setStructData(res.data.struct.data);
            setStructName(res.data.struct.name);
            setProjectType(res.data.type);
            if (res.data.node === null) {
                CreateNewNodeData();
            } else {
                GetNodeData();
            }
            if (res.data.code === null) {
                CreateNewCodeData();
            } else {
                GetCodeData();
            }
        })
        .catch((err) => {
            console.log(err);
        });
    }, [listid, CreateNewNodeData, CreateNewCodeData, GetNodeData, GetCodeData]);

    async function GenerateOperationCode(operation, index, operationIndex) {
        let sourceCode;
        if(projectType === 'single') {
            sourceCode = new SingleLinkCodeGenerator(structData, structName);
        }
        if (projectType === 'double') {
            sourceCode = new DoubleLinkCodeGenerator(structData, structName);
        }
        if (projectType === 'circular')  {
            sourceCode = new CircularLinkCodeGenerator(structData, structName);
        }

        let operationArr = [...operations];
        if(!operations.includes(operation)) {
            operationArr.push(operation);
            setOperations(operationArr);
        }

        let operateMainFunction = sourceCode.OperateMainFunction(operation, valueData[index], operationIndex, nodeRef.current.searchLog);
        let logArr = [...operationLogs];
        logArr.push(operateMainFunction);
        setOperationLogs(logArr);

        let mainFunction = sourceCode.GenerateMainFunction(logArr);

        let code = sourceCode.GenerateSourceCode(operationArr, nodeRef.current.searchLog) + mainFunction;
        setCode(code);
        UpdateCodeData(code, logArr, operationArr);
    }

    function ResetLogs(callback) {
        setCode('');
        let emptyArr = [];
        setOperations(emptyArr);
        setOperationLogs(emptyArr);
        callback(-1);
    }

    useEffect(() => {
        getInitialStructData();
    }, []);

    return(
        <div className="container mx-auto">
            <ToolEditor
                structData={structData}
                projectType={projectType}
                AddNewNode={(callback) => nodeRef.current.AddNewNode(callback)}
                InsertNodeInIndex={
                    (insertIndex, type, callback) => 
                    nodeRef.current.InsertNodeInIndex(insertIndex, type, callback)
                }
                SearchNodeInIndex={
                    (searchIndex, valueBy, callback) =>
                    nodeRef.current.SearchNodeInIndex(searchIndex, valueBy, callback)
                }
                RemoveNodeInIndex={
                    (deleteIndex, callback) =>
                    nodeRef.current.RemoveNodeInIndex(deleteIndex, callback)
                }
                ResetLogs={ResetLogs}
                RemoveAllNode={(callback) => nodeRef.current.RemoveAllNode(callback)}
            />

            <CodeEditor 
                code={code}
            />

            <NodeEditor ref={nodeRef}
                valueData={valueData}
                setValueData={setValueData}
                structData={structData}
                projectType={projectType}
                GenerateOperationCode={GenerateOperationCode}
            />
        </div>
    );
}