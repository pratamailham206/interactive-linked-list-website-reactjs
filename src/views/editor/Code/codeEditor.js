import React, { useState } from 'react';
import CodeSection from './codeSection';
import CodeModal from './codeModal';

export default function CodeEditor({ code }) {
    const [ showModal, setShowModal ] = useState(false);
    
    return(
        <div className="absolute right-10 bottom-10">
            <CodeSection
                callback={() => setShowModal(true)}
                code={code}
            />
            {showModal ?
                <CodeModal 
                    closeModal={() => setShowModal(false)} 
                    code={code}
                /> 
                : null
            }
        </div>
    );
}