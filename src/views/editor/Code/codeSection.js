import React from 'react';
import SyntaxHighlighter from 'react-syntax-highlighter';
import { docco } from 'react-syntax-highlighter/dist/esm/styles/hljs';

export default function CodeSection({ callback, code }) {
    return(
        <div className="bg-yellow-main w-84 rounded-xl">
            <div onClick={callback} className="cursor-pointer bg-orange-main flex items-center p-4 space-x-4 rounded-xl">
                <img className="w-8" alt="insert" src="/static/icons/coding.png" />
                <p className="font-bold text-xl font-source text-black">Source Code</p>
            </div>
            <SyntaxHighlighter language="cpp" style={docco}>
                    {code.substring(0, 234)}
            </SyntaxHighlighter>
        </div>
    );
}