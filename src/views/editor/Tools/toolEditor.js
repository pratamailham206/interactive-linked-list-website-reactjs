import React, { useState } from 'react';
import InsertNode from './insertNode';
import RemoveNode from './removeNode';
import SearchNode from './searchNode';
import { useHistory } from "react-router-dom";

export default function ToolEditor({
    structData,
    projectType,
    AddNewNode,
    InsertNodeInIndex,
    SearchNodeInIndex,
    RemoveNodeInIndex,
    ResetLogs,
    RemoveAllNode
}) {
    
    const [ toolModal, setToolModal ] = useState(-1);
    const history = useHistory();

    function SetToolsModal(index) {
        if(toolModal === index) {
            setToolModal(-1);
        } else {
            setToolModal(index);
        }
    }

    return(
        <div className="absolute left-0 top-100 flex space-x-2 items-end">
            <div>
                <div className="bg-yellow-main p-4 w-16">
                    <button title='dashboard' onClick={() => {history.push('/dashboard')}}><img className="w-8 cursor-pointer" alt="dashboard" src="/static/icons/dashboard.svg" /></button>
                </div>
                <div className="bg-yellow-second p-4 space-y-8 w-16">
                    <button title='Add New Node' onClick={() => AddNewNode(() => { setToolModal(-1) })}><img  className="w-8 cursor-pointer" alt="add" src="/static/icons/layer.png" /></button>
                    <button title='Insert New Node' onClick={() => SetToolsModal(1)}><img className="w-8 cursor-pointer" alt="insert" src="/static/icons/process.png" /></button>
                    <button title='Search Node' onClick={() => SetToolsModal(2)}><img className="w-8 cursor-pointer" alt="delete" src="/static/icons/magnifying-glass.png" /></button>
                    <button title='Remove Node Index' onClick={() => SetToolsModal(3)}><img className="w-8 cursor-pointer" alt="search" src="/static/icons/trash.png" /></button>
                    <button title='Remove Logs' onClick={() => ResetLogs(setToolModal)}><img className="w-8 cursor-pointer" alt="search" src="/static/icons/remove-database.png" /></button>
                    <button title='Remove All Node' onClick={() => RemoveAllNode(setToolModal)}><img className="w-8 cursor-pointer" alt="search" src="/static/icons/eraser.png" /></button>
                </div>
            </div>
            <div>
                {toolModal === 1 ? 
                    <InsertNode
                        InsertNodeInIndex={InsertNodeInIndex}
                        setToolModal={setToolModal}
                    /> : null
                }
                {toolModal === 2 ? 
                    <SearchNode
                        structData={structData}
                        projectType={projectType}
                        SearchNodeInIndex={SearchNodeInIndex}
                        setToolModal={setToolModal}
                    /> : null
                }
                {toolModal === 3 ? 
                    <RemoveNode
                        RemoveNodeInIndex={RemoveNodeInIndex}
                        setToolModal={setToolModal}
                    /> : null
                }
            </div>
        </div>
    );
}