import React, { useState } from 'react';
import { CheckRegexValidation } from '../../../regex/regex';

export default function RemoveNode({ RemoveNodeInIndex, setToolModal }) {
    const [ deleteIndex, setDeleteIndex ] = useState('');

    function HandleChange(e) {
        if (e.target.value === '' || CheckRegexValidation("int").test(e.target.value)) {
            setDeleteIndex(e.target.value)
        }
    }

    function RemoveNodeAtIndex() {
        RemoveNodeInIndex(deleteIndex, setToolModal);
        setDeleteIndex('');
    }

    return(
        <div className="bg-yellow-main p-4 mt-2 space-y-2">
            <p className="font-source text-md font-bold text-black">Delete in Index</p>
            <input value={deleteIndex} onChange={e => HandleChange(e)} placeholder="index" className="focus:outline-none focus:border-orang-main p-4 h-5 w-44 border rounded-xl"></input>
            <br />
            <button onClick={RemoveNodeAtIndex} className="text-xs font-bold font-playfair py-2 px-4 bg-orange-main hover:bg-orane-second text-white-main hover:text-black-main transition duration-300">submit</button>
        </div>
    );
}