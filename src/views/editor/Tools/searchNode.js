import React, { useState } from 'react';
import { CheckRegexValidation } from '../../../regex/regex';

export default function SearchNode({ 
    structData,
    projectType,
    SearchNodeInIndex,
    setToolModal 
}) {

    const [ searchIndex, setSearchIndex ] = useState(GenerateArray());

    function GenerateArray() {
        let array = []
        for (let i = 0; i < structData.length; i++) {
            array.push('');
        }
        return array;
    }

    function HandleChange(e, index, type) {
        let re = CheckRegexValidation(type);
        if (e.target.value === '' || re.test(e.target.value)) {
            let array = [...searchIndex];
            array[index] = e.target.value;
            setSearchIndex(array);
        }
    }

    function SearchNodeAtIndex(valueBy, index) {
        SearchNodeInIndex(searchIndex[index], valueBy, setToolModal);
        let array = [...searchIndex];
        array[index] = -1;
        setSearchIndex(array);
    }

    const checkToolValue = (si) => {
        if (projectType === 'single' || projectType === 'circular') {
            return si !== 0;
        }
        if (projectType === 'double') {
            return si > 1;
        }
    }

    return(
        <div className="bg-yellow-main p-4 mt-2 space-y-2">
            <div className="grid grid-cols-2 gap-4">
                {structData.map((variable, si) => {
                    return(
                        checkToolValue(si) ?
                        <div key={si} className="space-y-2">
                            <p className="font-source text-md font-bold text-black">Search by {variable.value}</p>
                            <input value={searchIndex[si]} onChange={e => HandleChange(e, si, variable.type)} placeholder="value" className="focus:outline-none focus:border-orang-main p-4 h-5 w-44 border rounded-xl"></input>
                            <br />
                            <button onClick={() => SearchNodeAtIndex(variable.value, si)} className="text-xs font-bold font-playfair py-2 px-4 bg-orange-main hover:bg-orane-second text-white-main hover:text-black-main transition duration-300">submit</button>
                        </div>
                        : null
                    );
                })}
            </div>
        </div>
    );
}