import React, { useState } from 'react';

export default function AddNode() {
    return(
        <div className="bg-yellow-main p-4 mt-2 space-y-2">
            <p className="font-source text-md font-bold text-black">Insert in Index</p>
            <input onChange={e => setInsertIndex(e.target.value)} placeholder="index" className="focus:outline-none focus:border-orang-main p-4 h-5 w-44 border rounded-xl"></input>
            <br />
            <button onClick={InsertNodeInIndex} className="text-xs font-bold font-playfair py-2 px-4 bg-orange-main hover:bg-orane-second text-white-main hover:text-black-main transition duration-300">submit</button>
        </div>
    );
}