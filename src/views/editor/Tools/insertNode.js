import React, { useState } from 'react';
import { CheckRegexValidation } from '../../../regex/regex';

export default function InsertNode({ InsertNodeInIndex, setToolModal }) {
    
    const [ insertBeforeIndex, setInsertBeforeIndex ] = useState('');
    const [ insertAfterIndex, setInsertAfterIndex ] = useState('');

    function InsertNodeBeforeIndex() {
        InsertNodeInIndex(insertBeforeIndex, 'before', setToolModal);
        setInsertBeforeIndex(-1);
    }

    function InsertNodeAfterIndex() {
        var index = Number(insertAfterIndex) + 1;
        InsertNodeInIndex(index, 'after', setToolModal);
        setInsertAfterIndex(-1);
    }

    function handleChangeInsertBefore(e) {
        if (e.target.value === '' || CheckRegexValidation("int").test(e.target.value)) {
            setInsertBeforeIndex(e.target.value)
        }
    }

    function handleChangeInsertAfter(e) {
        if (e.target.value === '' || CheckRegexValidation("int").test(e.target.value)) {
            setInsertAfterIndex(e.target.value)
        }
     } 

    return(
        <div className="bg-yellow-main p-4 mt-2 space-y-2">
            <p className="font-source text-md font-bold text-black">Insert before Index</p>
            <input type="text" value={insertBeforeIndex} onChange={handleChangeInsertBefore} placeholder="index" className="focus:outline-none focus:border-orang-main p-4 h-5 w-44 border rounded-xl"></input>
            <br />
            <button onClick={InsertNodeBeforeIndex} className="text-xs font-bold font-playfair py-2 px-4 bg-orange-main hover:bg-orane-second text-white-main hover:text-black-main transition duration-300">submit</button>
            <p className="font-source text-md font-bold text-black">Insert after Index</p>
            <input type="text" value={insertAfterIndex} onChange={handleChangeInsertAfter} placeholder="index" className="focus:outline-none focus:border-orang-main p-4 h-5 w-44 border rounded-xl"></input>
            <br />
            <button onClick={InsertNodeAfterIndex} className="text-xs font-bold font-playfair py-2 px-4 bg-orange-main hover:bg-orane-second text-white-main hover:text-black-main transition duration-300">submit</button>
        </div>
    );
}