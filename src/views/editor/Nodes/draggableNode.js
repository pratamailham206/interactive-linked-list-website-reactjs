import React, { useEffect, useRef } from 'react';
import gsap from 'gsap'
import { CheckRegexValidation } from '../../../regex/regex';

import Draggable from 'react-draggable';
import NodeEditModal from './nodeEditModal';
import NodeModal from './nodeModal';
import {useXarrow} from 'react-xarrows';

export default function DraggableNode({
    refs,
    id,
    structData,
    projectType,
    submitStructForm,
    ind,
    value,
    valueData,
    setValueData,
    dataShowed,
    setDataShowed,
    editModal
}) {
    const updateXarrow = useXarrow();
    const showModalEl = useRef();
    const editModalEl = useRef();
    const nodeEl = useRef();

    function ShowDataInIndex(index) {
        if(index !== dataShowed) {
            setDataShowed(index);
        } else {
            hideModal(showModalEl, () => setDataShowed(-1));
        }
    }
    
    function updateValueData (prop, event, index) {
        const re = CheckRegexValidation(prop.type);
        if (event.target.value === '' || re.test(event.target.value)) {
            const old = valueData[index];
            const updated = { ...old, [prop.value]: event.target.value }
            const clone = [...valueData];
            clone[index] = updated;
            setValueData(clone);
        }
    }

    // Animation
    const hideModal = (element, callback) => {
        gsap.timeline()
        .fromTo(element.current, {
            opacity: 1,
        }, {
            opacity: 0,
            y: 0,
            duration: 0.5,
            onComplete: callback
        });
    };

    const showModal = (element) => {
        gsap.timeline()
        .fromTo(element.current, {
            opacity: 0,
        }, {
            opacity: 1,
            y: 10,
            duration: 0.5,
        });
    }

    useEffect(() => {     
        if(showModalEl.current) {
            showModal(showModalEl);
        }
        if(editModalEl.current) {
            showModal(editModalEl);
        }   
    }, [dataShowed, editModal, ind]);

    return(
        <Draggable handle="button" onDrag={updateXarrow} onStop={updateXarrow}>
            <div ref={refs} className='w-52'>
                <div className="flex justify-center ">
                    <button ref={nodeEl} id={id} onClick={() => ShowDataInIndex(ind)} 
                        className={`focus:outline-none cursore-pointer py-4 px-12 text-xl text-white rounded-xl 
                            ${ editModal === ind || dataShowed === ind ? 'bg-purple-main' : 'bg-purple-second'}`}>{ind}</button>
                </div>
                {editModal === ind ?
                    <NodeEditModal
                        refs={editModalEl}
                        structData={structData}
                        projectType={projectType}
                        value={value}
                        valueData={valueData}
                        ind={ind}
                        updateValueData={updateValueData}
                        submitStructForm={submitStructForm}
                    />
                : null }
                {dataShowed === ind ? 
                    <NodeModal
                        refs={showModalEl}
                        structData={structData}
                        projectType={projectType}
                        value={value}
                        valueData={valueData} 
                        ind={ind}
                    />
                : null }
            </div>
        </Draggable>
    );
}