import React from 'react';

export default function NodeModal({
    refs,
    structData, 
    projectType,
    value, 
    valueData, 
    ind
}) {
    const inputDisabledValue = (disabledValue) => {
        return <input disabled value={disabledValue} className="focus:outline-none focus:border-purple-main p-4 h-5 w-44 border rounded-xl"></input>
    }

    const structNodeValue = (variable, i) => {
        if (projectType === 'single') {
            if (i === 0) {
                return inputDisabledValue(valueData[ind + 1] ? ind + 1 : "NULL");
            } else {
                return inputDisabledValue(value[variable.value] || '');
            }
        } 
        if (projectType === 'double') {
            if (i === 0) {
                return inputDisabledValue(valueData[ind + 1] ? ind + 1 : "NULL");
            }
            if (i === 1) {
                return inputDisabledValue(valueData[ind - 1] ? ind - 1 : "NULL");
            }
            else {
                return inputDisabledValue(value[variable.value] || '');
            }
        }
        if (projectType === 'circular') {
            if (i === 0) {
                return inputDisabledValue(valueData[ind + 1] ? ind + 1 : 0);
            } else {
                return inputDisabledValue(value[variable.value] || '');
            }
        }
    }

    return(
        <div ref={refs} className="py-4 px-4 bg-purple-second rounded-xl space-y-4">
            {structData.map((variable, i) => {
                return(
                    <div key={i} className="space-y-1">
                        <label className="font-source text-lg">{variable.value}</label>
                        <br />
                        {structNodeValue(variable, i)}
                    </div>
                );
            })}
        </div> 
    );
}