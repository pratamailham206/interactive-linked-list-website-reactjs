import React from 'react';

export default function NodeEditModal({ 
    refs,
    structData, 
    projectType,
    value, 
    valueData, 
    ind, 
    updateValueData, 
    submitStructForm 
}) {
    const inputDisabledValue = (disabledValue, placeholder) => {
        return <input disabled value={disabledValue} placeholder={placeholder} className="focus:outline-none focus:border-purple-main p-4 h-5 w-44 border rounded-xl"></input>
    }

    const inputValue = (variable) => {
        return <input  value={value[variable.value] || ''} onChange={e => updateValueData(variable, e, ind)} placeholder={variable.type} className="focus:outline-none focus:border-purple-main p-4 h-5 w-44 border rounded-xl"></input>
    }

    const structNodeValue = (variable, i) => {
        if (projectType === 'single') {
            if (i === 0) {
                return inputDisabledValue(valueData[ind + 1] ? ind + 1 : "NULL", variable.type);
            } else {
                return inputValue(variable);
            }
        } 
        if (projectType === 'double') {
            if (i === 0) {
                return inputDisabledValue(valueData[ind + 1] ? ind + 1 : "NULL", variable.type);
            }
            if (i === 1) {
                return inputDisabledValue(valueData[ind - 1] ? ind - 1 : "NULL", variable.type);
            }
            else {
                return inputValue(variable);
            }
        }
        if (projectType === 'circular') {
            if (i === 0) {
                return inputDisabledValue(valueData[ind + 1] ? ind + 1 : 0, variable.type);
            } else {
                return inputValue(variable);
            }
        }
    }

    return(
        <div ref={refs} className="py-4 px-4 bg-purple-third rounded-xl space-y-4">
            {structData.map((variable, i) => {
                return(
                    <div key={i} className="space-y-1">
                        <label className="font-source text-lg">{variable.value}</label>
                        <br />
                        {structNodeValue(variable, i)}
                    </div>
                );
            })}
            <button onClick={e => submitStructForm(e, ind)} className="text-xs font-bold font-playfair py-2 px-4 bg-purple-main hover:bg-purple-second text-white-main hover:text-black-main transition duration-300">submit</button>
        </div> 
    );
}