import React, { forwardRef, useState, useImperativeHandle, createRef } from 'react';
import DraggableNode from './draggableNode';
import Xarrow, {Xwrapper} from 'react-xarrows';
import gsap from 'gsap';

const NodeSection = forwardRef(
    ({
        projectType,
        structData,
        submitStructForm,
        valueData,
        setValueData,
        editModal, 
        dataShowed, 
        setDataShowed,
    }, ref) => {

    const [nodeRef, setNodeRef] = useState([]);
    const [arrowRef, setArrowRef] = useState([]);

    React.useEffect(() => {
        setNodeRef(nodeRef => (
          Array(valueData.length).fill().map((_, i) => nodeRef[i] || createRef())
        ));
        setArrowRef(arrowRef => (
            Array(valueData.length).fill().map((_, i) => arrowRef[i] || createRef())
        ));
    }, [valueData.length]);

    const nodeAnimation = (index, initialValue, value, callback) => {
        gsap.timeline()
        .from(nodeRef[index].current, {
            startAt: {
                opacity: initialValue,
                scale: initialValue
            },
            duration: 0
        })
        .to(nodeRef[index].current, {
            opacity: value,
            scale: value,
            ease: "power1.inOut",
            onComplete: () => {
                if (value === 0) {
                    restoreNodeAnim(index, 1, callback);
                    restoreArrowAnim(index, 1);
                }
                else {
                    callback();
                }
            }
        });
    }
    
    async function restoreNodeAnim(index, value, callback) {
        gsap.to(nodeRef[index].current, {
            opacity: value,
            scale: value,
            duration: 0,
            onComplete: callback
        })
    }

    const arrowAnimation = (index, initialValue,  value, callback) => {
        gsap.timeline()
        .from(nodeRef[index].current, {
            startAt: {
                opacity: initialValue,
                scale: initialValue
            },
            duration: 0
        })
        .from(arrowRef[index].current, {
            startAt: {
                opacity: initialValue,
            },
            duration: 0
        })
        .to(arrowRef[index].current, {
            opacity: value,
            ease: "linear.inOut",
            onComplete: callback
        });
    }

    function restoreArrowAnim(index, value) {
        if (arrowRef[index].current === null) {
            return;
        }
        gsap.to(arrowRef[index].current, {
            opacity: value,
            duration: 0
        })
    }

    useImperativeHandle(ref, () => ({
        nodeAnimation,
        arrowAnimation
    }));

    const renderArrowType = (ind) => {
        const tAnchor = { position: "auto", offset: { y: -10 } }
        const bAnchor = { position: "auto", offset: { y: 10 } }

        if (projectType === "single") {
            return (
                ind !== 0 && ind < valueData.length ?
                <div key={ind}>
                    <div
                        ref={arrowRef[ind]}>
                        <Xarrow
                            strokeWidth={2} 
                            color='#8424BD' 
                            start={`elem${ind - 1}`} 
                            end={`elem${ind}`} />
                    </div>
                </div> 
                : null
            );
        }
        if (projectType === "double") {
            return (
                ind !== 0 && ind < valueData.length ?
                <div key={ind}>
                    <div
                        ref={arrowRef[ind]}
                        className="space-y-2">
                        <Xarrow
                            strokeWidth={2} 
                            startAnchor={tAnchor}
                            endAnchor={tAnchor}
                            color='#8424BD' 
                            start={`elem${ind - 1}`} 
                            end={`elem${ind}`} />
                        <Xarrow
                            strokeWidth={2}
                            startAnchor={bAnchor}
                            endAnchor={bAnchor}
                            color='#8424BD' 
                            start={`elem${ind}`} 
                            end={`elem${ind - 1}`} />
                    </div>
                </div> 
                : null
            );
        }
        if (projectType === "circular") {
            return (
                valueData.length > 1 ?
                <div key={ind}>
                    <div
                        ref={arrowRef[ind]}>
                        <Xarrow
                            strokeWidth={2} 
                            color='#8424BD' 
                            start={`elem${ind}`} 
                            end={`elem${ind === valueData.length - 1 ? 0 : ind + 1}`}
                            startAnchor={ind === valueData.length - 1 ? 'top' : 'auto'}
                            endAnchor={ind === valueData.length - 1 ? 'top' : 'auto'}
                            _cpy1Offset={ind === valueData.length - 1 ? -100 : 0}
                            _cpy2Offset={ind === valueData.length - 1 ? -100 : 0}
                            />
                    </div>
                </div> 
                : null
            ); 
        }
    }

    return(
        <div className="mx-auto w-4/5 flex justify-evenly">
            <Xwrapper>
                {valueData.map((value, ind) => {
                    return(
                        <DraggableNode
                            key={ind}
                            refs={nodeRef[ind]}
                            id={`elem${ind}`}
                            structData={structData}
                            projectType={projectType}
                            submitStructForm={submitStructForm}
                            ind={ind}
                            value={value}
                            valueData={valueData}
                            setValueData={setValueData}
                            dataShowed={dataShowed}
                            setDataShowed={setDataShowed}
                            editModal={editModal}
                        />
                    )
                })}
                {valueData.map((value, ind) => {
                    return(
                        renderArrowType(ind)
                    )
                })}
            </Xwrapper>
        </div>
    );
})

export default NodeSection;