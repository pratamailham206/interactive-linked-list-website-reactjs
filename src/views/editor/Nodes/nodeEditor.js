import React, { useState, forwardRef, useImperativeHandle, useRef } from 'react';
import { useParams } from 'react-router-dom';
import axiosInstance from '../../../axios';
import NodeSection from './nodeSection';

const NodeEditor = forwardRef(
    ({
        valueData,
        setValueData,
        structData,
        projectType,
        GenerateOperationCode
    }, ref) => {

    const nodeSectionRef = useRef();
    let { listid } = useParams();

    const [ editModal, setEditModal ] = useState(-1);
    const [ dataShowed, setDataShowed ] = useState(-1);
    
    const [ insert, setInsert ] = useState({
        type: '',
        value: -1
    });
    const [ result, setResult ] = useState(-1);
    const [ searchLog, setSearchLog ] = useState('');

    function UpdateValueData(_data) {
        axiosInstance.put('/node/update', {
            data: _data,
            listId: listid
        }).then((res) => {
            console.log("update value success");
        }).catch((err) => {
            console.log(err);
        });
    }

    async function AddNewNode(callback) {
        let array = [...valueData];
        array.push({});
        let index = array.length - 1;
        
        if(index >= 0) {
            await setValueData(array);
            if(array.length > 1) {
                nodeSectionRef.current.arrowAnimation(index, 0, 1, () => {
                    nodeSectionRef.current.nodeAnimation(index, 0, 1, () => {
                        setEditModal(index);
                    })
                })
            }
            else {
                nodeSectionRef.current.nodeAnimation(index, 0, 1, () => {
                    setEditModal(index);
                });
            }
        }

        callback();
    }
    
    async function InsertNodeInIndex(insertIndex, types, callback) {
        let array = [...valueData];
        const index = array.length - (array.length - insertIndex);

        if (insertIndex !== -1) {
            array.splice(insertIndex, 0, {});
            setInsert({
                type: types,
                value: insertIndex
            });
            await setValueData(array);
            if(insertIndex === "0") {
                nodeSectionRef.current.nodeAnimation(index, 0, 1, () => {
                    setEditModal(index);
                });
            }
            else if(array.length > 1) {
                nodeSectionRef.current.arrowAnimation(index, 0, 1, () => {
                    nodeSectionRef.current.nodeAnimation(index, 0, 1, () => {
                        setEditModal(index);
                    })
                })
            }
            callback(-1);
        }
    }

    async function SearchNodeInIndex(searchIndex, valueBy, callback) {
        await setSearchLog(valueBy);
        for (let i = 0; i < valueData.length; i++) {
            if (valueData[i][valueBy] === searchIndex) {
                setResult(i);
                setDataShowed(i);
                GenerateOperationCode('search', i, searchIndex, valueBy);
            }
        }
        callback(-1);
    }

    function RemoveNodeInIndex(deleteIndex, callback) {
        let array = [...valueData];
        
        if (deleteIndex !== '') {
            array.splice(deleteIndex, 1);
            if(array.length > 1 && deleteIndex !== "0") {
                nodeSectionRef.current.arrowAnimation(deleteIndex, 1, 0, () => {
                    nodeSectionRef.current.nodeAnimation(deleteIndex, 1, 0, () => {
                        setValueData(array);
                        UpdateValueData(array);
                    })
                })
            }
            else {
                nodeSectionRef.current.nodeAnimation(deleteIndex, 1, 0, () => {
                    setValueData(array);
                    UpdateValueData(array);
                });
            }
            GenerateOperationCode('delete', deleteIndex, deleteIndex);
            callback(-1)
        }
    }

    function GenerateArray(data) {
        let array = []
        for (let i = 0; i <= Number(data); i++) {
            array.push('');
        }
        return array;
    }

    function RemoveAllNode(callback, i) {
        if (valueData.length <= 0) {
            return;
        }
        
        if (i < 0) {
            callback(-1);
            setDataShowed(-1);
            return;
        }
        if (i === undefined) {
            i = valueData.length - 1;
        }
        let array = GenerateArray(i);
        let dataIndex = Number(i - 1);

        if(i > 0) {
            nodeSectionRef.current.arrowAnimation(i, 1, 0, () => {
                nodeSectionRef.current.nodeAnimation(i, 1, 0, async () => {
                    array.pop();
                    await setValueData(array);
                    RemoveAllNode(callback, dataIndex);
                })
            })
        }
        else {
            nodeSectionRef.current.nodeAnimation(i, 1, 0, async () => {
                setValueData([]);
                UpdateValueData([]);
            });
        }
    }

    useImperativeHandle(ref, () => ({
        AddNewNode,
        InsertNodeInIndex,
        SearchNodeInIndex,
        RemoveNodeInIndex,
        RemoveAllNode,
        searchLog
    }));

        
    function submitStructForm(event, index) {
        event.preventDefault();
        if (insert.value !== -1) {
            GenerateOperationCode(insert.type, index, index);
            setInsert({
                type: '',
                value: -1
            });
        } else {
            GenerateOperationCode('add', index, index);
        }
        setEditModal(-1);
        UpdateValueData(valueData);
    }

    return(
        <NodeSection
            ref={nodeSectionRef}
            projectType={projectType}
            structData={structData}
            submitStructForm={submitStructForm}
            valueData={valueData}
            setValueData={setValueData}
            editModal={editModal}
            dataShowed={dataShowed}
            setDataShowed={setDataShowed}
            result={result}
        />
    );
})

export default NodeEditor;