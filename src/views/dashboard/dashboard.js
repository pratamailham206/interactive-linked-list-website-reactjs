import React, { useState, useEffect, useCallback } from 'react';
import Cookies from 'universal-cookie';
import { useHistory, Link } from 'react-router-dom';
import axiosInstance from '../../axios';
import { useAuth } from '../../authentication/auth';


export default function DashboardView() {
    const cookies = new Cookies();
    const history = useHistory();
    let auth = useAuth();
    const [ lists, setLists ] = useState({ isLoading: true, data: [] });

    function handleProjectType(type) {
        axiosInstance
        .post('list/create', {
            type: type
        })
        .then((res) => {
            const listId = res.data.data.id;
            cookies.set('projecttype', type, { path: '/' });
            history.push(`/struct/${type}/${listId}`);
        })
        .catch((err) => {
            console.log(err);
        });
    }

    const fetchListData = useCallback(() => {
        axiosInstance
        .get('list/my-lists')
        .then((res) => {
            setLists({
                isLoading: false,
                data: res.data
            });
        })
        .catch((err) => {
            console.log(err);
        })
    }, []);

    useEffect(() => {
        if (lists.isLoading && auth.user) {
            fetchListData();
        }
        if (!auth.user) {
            history.push('/login');
        }
    }, [auth.user, history, lists.isLoading, fetchListData]);
    
    return(
        <div className="container mx-auto px-12 md:px-24">
            <div className="grid grid-cols-3 divide-x divide-gray-500 gap-16">
                <div className="space-y-10">
                    <div className="space-y-4">
                        <h1 className="font-playfair text-4xl font-bold">Create Linked-List</h1>
                        <p className="font-source text-lg">Create new Linked-List project, and select which type of Linked-List. </p>
                    </div>
                    <div className="space-y-6">
                        <div onClick={() => handleProjectType("single")} className="cursor-pointer py-4 px-6 border rounded-xl hover:border-yellow-main transition duration-300">
                            <div className="flex items-center space-x-8">
                                <img className="w-12" src="/static/icons/document.png" alt="single-link" />
                                <h1 className="font-source text-xl">Single Linked-List</h1>
                                <div className="flex justify-end w-auto">
                                    <img className="w-4" src="/static/icons/plus.png" alt="plus" />
                                </div>
                            </div>
                        </div>
                        <div onClick={() => handleProjectType("double")} className="cursor-pointer py-4 px-6 border rounded-xl hover:border-yellow-main transition duration-300">
                            <div className="flex items-center space-x-8">
                                <img className="w-12" src="/static/icons/business-card.png" alt="single-link" />
                                <h1 className="font-source text-xl">Double Linked-List</h1>
                                <div className="flex justify-end w-auto">
                                    <img className="w-4" src="/static/icons/plus.png" alt="plus" />
                                </div>
                            </div>
                        </div>
                        <div onClick={() => handleProjectType("circular")} className="cursor-pointer py-4 px-6 border rounded-xl hover:border-yellow-main transition duration-300">
                            <div className="flex items-center space-x-8">
                                <img className="w-12" src="/static/icons/zip-folder.png" alt="single-link" />
                                <h1 className="font-source text-xl">Circular Linked-List</h1>
                                <div className="flex justify-end w-auto">
                                    <img className="w-4" src="/static/icons/plus.png" alt="plus" />
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div className="col-span-2">
                    <div className="pl-16 space-y-10">
                        <div className="space-y-8">
                            <div className="space-y-4">
                                <h1 className="font-playfair text-4xl font-bold">Saved Linked-List Project</h1>
                                <p className="font-source text-lg">select the linked list project that has been saved.</p>
                            </div>
                            <div className="grid grid-cols-3 gap-6">
                                {lists.isLoading ? null :
                                    lists.data.length === 0 ? <h1 className="font-source text-xl">Linked List project is empty</h1> :
                                    lists.data.map((list, i) => {
                                        return(
                                            <Link to={`/struct/${list.type}/${list.id}`} key={i} className="py-4 px-6 border rounded-xl hover:border-yellow-main transition duration-300">
                                                <div className="flex justify-end w-auto">
                                                    <img className="w-4" src="/static/icons/plus.png" alt="plus" />
                                                </div>
                                                <div className="my-6 flex justify-center w-auto">
                                                    <img className="w-12" src="/static/icons/zip-folder.png" alt="single-link" />
                                                </div>
                                                <h1 className="font-source text-xl">Circular Linked-List</h1>
                                                <h2 className="font-source opacity-50 text-md">12 Maret 2022</h2>
                                            </Link>
                                        );
                                    })
                                }
                                
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    );
}