import React, { useEffect, useState, useCallback } from 'react';
import { useParams, useHistory } from "react-router-dom";
import axiosInstance from '../../axios';

export default function Struct() {
    let { type } = useParams();
    let { listid } = useParams();
    const history = useHistory();
    const [ structName, setStructName ] = useState("");
    const [ index, setIndex ] = useState(2);
    const [ formData, setFormData ] = useState([{
        type: '',
        value: '',
    }]);
    const [ projectType, setProjectType ] = useState("");
    const [ error, setError ] = useState({});

    function handleStructName(event) {
        setStructName(event.target.value);
    }

    function addNewForm(event) {
        event.preventDefault();
        setIndex(index + 1);
        var formObj = {};
        formObj['type'] = 'int';
        formObj['value'] = "";
        let newFormArray = [...formData];
        newFormArray.push(formObj);
        setFormData(newFormArray);
    }

    function updateItem (prop, event, indx) {
        const old = formData[indx];
        const updated = { ...old, [prop]: event.target.value }
        const clone = [...formData];
        clone[indx] = updated;
        setFormData(clone);
    }

    function handleValidation() {
        let formIsValid = true;
        let errors = {};
        if (structName === "") {
            formIsValid = false;
            errors["name"] = "Struct name can't be empty";
        }
        for (let i = 0; i < formData.length; i++) {
            if (formData[i].value === "") {
                formIsValid = false;
                errors["value" + i] = "Struct name can't be empty";
            }
        }

        setError(errors);
        return formIsValid
    }

    function submitData(event) {
        event.preventDefault();
        if(handleValidation()) {
            axiosInstance
            .post('struct/create', {
                name: structName,
                data: formData,
                listId: listid
            })
            .then((res) => {
                history.push(`/editor/${res.data.data.listId}`);
            })
            .catch((err) => {
                console.log(err);
            });
        } else {
            console.log("Please correct error below");
        }
    }

    const initializeStruct = useCallback(() => { 
        setProjectType(type);
        if (type === 'single' || type === 'circular') {
            let jsonData = [{
                type: structName + "*",
                value: 'next',
            }, {
                type: 'int',
                value: '',
            }];
            setFormData(jsonData);
        }
        else if (type === 'double') {
            let jsonData = [{
                type: structName + "*",
                value: 'next',
            },
            {
                type: structName + "*",
                value: 'prev',
            },
            {
                type: 'int',
                value: '',
            }];
            setFormData(jsonData);
        }
    }, [type, structName]);

    const checkIfStructExist = useCallback(() => {
        axiosInstance
        .get(`/list/detail/${listid}`)
        .then((res) => {
            const list = res.data;
            if (list.struct === null)
                initializeStruct();
            else
                history.push(`/editor/${listid}`);
        })
        .catch((err) => {
            console.log(err);
        });
    }, [history, listid, initializeStruct]);

    useEffect(() => {
        checkIfStructExist();
    }, [checkIfStructExist]);

    return(
        <div className="container mb-12 px-12 md:px-52">
            <div className="space-y-8">
                <div className="space-y-2">
                    <h1 className="font-playfair font-bold text-4xl">Create Struct</h1>
                    <p className="font-source text-lg">Create new Struct and insert variable data to struct that used in Linked-List.</p>
                </div>
                <form className="space-y-4">
                    <div className="space-y-3">
                        <label className="font-source text-xl">Struct Name</label>
                        <br />
                        <input onChange={e => handleStructName(e)} className="w-7/12 h-12 border focus:outline-none focus:border-yellow-main p-4"></input>
                        <br />
                        <span style={{ color: "red" }}>{error["name"]}</span>
                    </div>
                    {formData.map((data, i) => {
                        return(
                            <div key={i} className="w-full flex items-cente space-y-2">
                                <div className="pt-2">
                                    <label className="font-source text-lg focus:outline-none focus:border-yellow-main">Variable Name</label>
                                    { i === 0 || (i === 1 && projectType === 'double') ?
                                    <select disabled className="p-3 border w-44" onChange={e => updateItem('type', e, i)} value={data.type} name="data-type" id="data-type">
                                        <option value={structName+ "*"}>{structName+ "*"}</option>
                                        <option value="int">Int</option>
                                        <option value="string">String</option>
                                        <option value="double">double</option>
                                        <option value="float">float</option>
                                    </select> :
                                    <select className="p-3 border w-44" onChange={e => updateItem('type', e, i)} value={data.type} name="data-type" id="data-type">
                                        <option value={structName+ "*"}>{structName+ "*"}</option>
                                        <option value="int">Int</option>
                                        <option value="string">String</option>
                                        <option value="double">double</option>
                                        <option value="float">float</option>
                                    </select> }
                                </div>
                                <div className="w-full">
                                    <label className="font-source text-lg">Variable Name</label>
                                    <br />
                                    {i === 0 || (i === 1 && projectType === 'double') ? 
                                    <input disabled onChange={e => updateItem('value', e, i)} className="focus:outline-none focus:border-yellow-main p-4 w-7/12 h-12 border" value={data.value}></input> :
                                    <input onChange={e => updateItem('value', e, i)} className="focus:outline-none focus:border-yellow-main p-4 w-7/12 h-12 border" value={data.value}></input> }
                                    <br />
                                    <span style={{ color: "red" }}>{error["value" + i]}</span>
                                </div>
                            </div>
                        );
                    })}
                    <div className="flex justify-end space-x-2 w-8/12">
                        <button onClick={(event) => addNewForm(event)} className="text-xs font-bold font-playfair py-3 px-7 text-orange-main border border-orange-main hover:bg-yellow-second hover:text-black-main transition duration-300">Add new data</button>
                        <button onClick={(event) => submitData(event)} className="text-xs font-bold font-playfair py-3 px-7 bg-orange-main hover:bg-yellow-second text-white-main hover:text-black-main transition duration-300">Submit</button>
                    </div>
                </form>
            </div>
        </div>
    );
}