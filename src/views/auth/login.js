import React, { useState, useEffect } from 'react';
import { useHistory, useLocation } from 'react-router-dom';
import { useAuth } from '../../authentication/auth';
import { loginValidation } from '../../validation/loginValidation';
import { LoginUser } from '../../authentication/loginUser';

export default function Login() {
    let location = useLocation();
    let auth = useAuth();
    const history = useHistory();
    let { from } = location.state || { from: { pathname: "/dashboard" } };

    const [ error, setError ] = useState({});
    const [ formData, setFormData ] = useState({
        email: "",
        password: ""
    });
    const [ loadingSubmit, setLoadingSubmit ] = useState(false);

    const handleChange = prop => event => {
        setFormData({ ...formData, [prop]: event.target.value });
    };

    function redirect(accessToken) {
        auth.signin(() => {
            history.replace(from);
        }, accessToken);
        setLoadingSubmit(false);
    }

    function setErrorData(errors) {
        setLoadingSubmit(false);
        setError(errors);
    }

    async function HandleSubmit(event) {
        event.preventDefault();
        setLoadingSubmit(true);
        let isValidate = await loginValidation(formData, setErrorData);
        if (isValidate) {
            LoginUser(formData, redirect, setErrorData);
        } else {
            console.log("Invalid Form Data");
        }
    };

    useEffect(() => {
        if (auth.user) {
            history.push('/dashboard');
        }
    }, [auth.user, history]);

    return(
        <div className="container mx-auto px-12 md:px-24">
            <div className="grid lg:grid-cols-2 md:grid-cols-1">
                <div className="text-right">
                    <img alt="hero" className="w-96" src="/static/images/auth.jpg" />
                </div>
                <div className="flex items-center">
                    <div className="space-y-8 w-full">
                        <div className="flex space-x-4">
                            <h1 className="font-playfair text-4xl font-bold">Login</h1>
                            <p className="font-playfair text-lg self-center">or</p>
                            <h1 className="underline font-playfair text-4xl">Register</h1>
                        </div>
                        <form className="space-y-2">
                            <div className="space-y-2">
                                <label className="font-source text-lg">Email</label>
                                <input onChange={handleChange('email')} className="focus:outline-none focus:border-yellow-main p-4 w-full h-12 border"></input>
                                <br />
                                <span style={{ color: "red" }}>{error["email"]}</span>
                            </div>
                            <div className="space-y-2">
                                <label className="font-source text-lg">Password</label>
                                <input type="password" required onChange={handleChange('password')} className="focus:outline-none focus:border-yellow-main p-4 w-full h-12 border"></input>
                                <br />
                                <span style={{ color: "red" }}>{error["password"]}</span>
                            </div>
                            <span style={{ color: "red" }}>{error["404"]}</span>
                            <br />
                            <div className="flex items-center space-x-3">
                                <button onClick={(event) => HandleSubmit(event)} className="text-xs font-bold font-playfair py-3 px-7 bg-yellow-main hover:bg-yellow-second text-white-main hover:text-black-main transition duration-300">Login</button>
                                {loadingSubmit && <svg className="animate-spin bg-black h-5 w-5 mr-3" viewBox="0 0 24 24"></svg> }
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    );
}