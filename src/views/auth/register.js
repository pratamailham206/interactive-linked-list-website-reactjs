import React, { useState, useEffect } from 'react';
import { useHistory, useLocation } from 'react-router-dom';
import { useAuth } from '../../authentication/auth';
import { LoginUser } from '../../authentication/loginUser';
import { RegisterUser } from '../../authentication/registerUser';
import { RegisterValidation } from '../../validation/registerValidation';

export default function Register() {
    let auth = useAuth();
    const history = useHistory();
    let location = useLocation();
    let { from } = location.state || { from: { pathname: "/dashboard" } };
    
    const [ error, setError ] = useState({});
    const [ formData, setFormData ] = useState({
        username: "",
        email: "",
        password: "",
        password2: ""
    });
    const [ loadingSubmit, setLoadingSubmit ] = useState(false);

    const handleChange = prop => event => {
        setFormData({ ...formData, [prop]: event.target.value });
    };
    
    function redirect(accessToken) {
        setLoadingSubmit(false);
        auth.signin(() => {
            history.replace(from);
        }, accessToken);
    }

    function setErrorData(errors) {
        setLoadingSubmit(false);
        setError(errors);
    }

    async function HandleSubmit(event) {
        event.preventDefault();
        setLoadingSubmit(true);
        let isValidate = await RegisterValidation(formData, setErrorData);
        if (isValidate) {
            RegisterUser(formData, LoginUser, setErrorData, redirect);
        } else {
            console.log("Invalid Form Data");
        }
    };

    useEffect(() => {
        if (auth.user) {
            history.push('/dashboard');
        }
    }, [auth.user, history]);

    return(
        <div className="container mx-auto px-12 md:px-24">
            <div className="grid lg:grid-cols-2 md:grid-cols-1">
                <div className="text-right">
                    <img alt="hero" className="w-96" src="/static/images/auth.jpg" />
                </div>
                <div className="space-y-2 h-full flex items-center">
                    <div className="space-y-8 w-full">
                        <div className="flex space-x-4">
                            <h1 className="font-playfair text-4xl font-bold">Register</h1>
                            <p className="font-playfair text-lg self-center">or</p>
                            <h1 className="underline font-playfair text-4xl">Login</h1>
                        </div>
                        <div className="flex items-center">
                            <form className="space-y-2 w-full">
                                <div className="space-y-2">
                                    <label className="font-source text-lg">Username</label>
                                    <input onChange={handleChange('username')} className="focus:outline-none focus:border-yellow-main p-4 w-full h-12 border"></input>
                                    <br />
                                    <span style={{ color: "red" }}>{error["username"]}</span>
                                </div>
                                <div className="space-y-2">
                                    <label className="font-source text-lg">Email</label>
                                    <input onChange={handleChange('email')} className="focus:outline-none focus:border-yellow-main p-4 w-full h-12 border"></input>
                                    <br />
                                    <span style={{ color: "red" }}>{error["email"]}</span>
                                </div>
                                <div className="space-y-2">
                                    <label className="font-source text-lg">Password</label>
                                    <input type="password" required onChange={handleChange('password')}  className="focus:outline-none focus:border-yellow-main p-4 w-full h-12 border"></input>
                                    <br />
                                    <span style={{ color: "red" }}>{error["password"]}</span>
                                </div>
                                <div className="space-y-2">
                                    <label className="font-source text-lg">Retype Password</label>
                                    <input type="password" required onChange={handleChange('password2')}  className="focus:outline-none focus:border-yellow-main p-4 w-full h-12 border"></input>
                                    <br />
                                    <span style={{ color: "red" }}>{error["password"]}</span>
                                    <br />
                                    <span style={{ color: "red" }}>{error["match"]}</span>
                                </div>
                                <span style={{ color: "red" }}>{error["404"]}</span>
                                <div className="flex items-center space-x-3">
                                    <button onClick={event => HandleSubmit(event)} className="text-xs font-bold font-playfair py-3 px-7 bg-yellow-main hover:bg-yellow-second text-white-main hover:text-black-main transition duration-300">Submit</button>
                                    {loadingSubmit && <svg className="animate-spin bg-black h-5 w-5 mr-3" viewBox="0 0 24 24"></svg> }
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    );
}