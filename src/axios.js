import axios from 'axios';

const baseURL = 'http://localhost:8000/api/';

const axiosInstance = axios.create({
	baseURL: baseURL,
	timeout: 100000,
	headers: {
		'x-access-token': localStorage.getItem('access_token') ?
			localStorage.getItem('access_token') :
			null,
		'Content-Type': 'application/json',
		accept: 'application/json',
	},
});

export default axiosInstance;