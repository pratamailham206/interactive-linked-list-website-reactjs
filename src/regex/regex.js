export function CheckRegexValidation(type) {
    const numRegex = /^[0-9\b]+$/;
    const floatRegex = /^\d{0,7}(\.\d{0,7}){0,1}$/;
    const letterNumRegex = /^(?:[A-Za-z]+)(?:[A-Za-z0-9 _]*)$/;
    const doubleRegex = /^\d{0,15}(\.\d{0,15}){0,1}$/;

    if (type === "int") {
        return numRegex;
    }
    if (type === "string") {
        return letterNumRegex;
    } 
    if (type === "float") {
        return floatRegex;
    }
    if (type === "double") {
        return doubleRegex;
    }
    else {
        return letterNumRegex;
    }
}