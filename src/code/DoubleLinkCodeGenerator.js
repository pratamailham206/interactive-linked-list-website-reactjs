export default class DoubleLinkCodeGenerator {
    constructor(structType, structName) {
        this._structType = structType;
        this._structName = structName;
    }

    head = `
    #include <stdio.h>
    #include <stdlib.h>
    #include <malloc.h>
    #include <iostream>
    #include <string>
    using namespace std;
    `;

    MapStruct() {
        let struct_type = ``;
        for (let i = 0; i < this._structType.length; i++) {
            struct_type = struct_type + `${this._structType[i].type} ${this._structType[i].value};`;
            if (i !== this._structType.length - 1) {
                struct_type = struct_type + `
        `;
            }
        }

        return struct_type;
    }
    
    GenerateStruct() {
        let struct = `
    struct ${this._structName} {
        ${this.MapStruct()}
    };

    struct ${this._structName} *start = NULL;
    `;
        return struct;
    }

    MapParam() {
        let struct_type_param = ``;
        for (let i = 0; i < this._structType.length; i++) {
            if (i > 1 ) {
                if (i !== this._structType.length - 1) {
                    struct_type_param = struct_type_param + `${this._structType[i].type} ${this._structType[i].value}, `;
                } else {
                    struct_type_param = struct_type_param + `${this._structType[i].type} ${this._structType[i].value}`;
                }
            }
        }

        return struct_type_param;
    }

    MapValue(new_node) {
        let struct_data = ``
        for (let i = 0; i < this._structType.length; i++) {
            if (i > 1) {
                struct_data = struct_data + `${new_node} -> ${this._structType[i].value} = ${this._structType[i].value};`;
                if (i !== this._structType.length - 1) {
                    struct_data = struct_data + `
        `;
                }
            }
        }
        return struct_data;
    }

    MapParamFunc(structData) {
        let struct_type_param = ``;
        let variableData = ``;
        for (let i = 0; i < this._structType.length; i++) {
            if (i > 1 ) {
                if(this._structType[i].type === 'string') {
                    variableData = `"${structData[this._structType[i].value]}"`;
                }
                else {
                    variableData = `${structData[this._structType[i].value]}`;
                }
                if (i !== this._structType.length - 1) {
                    struct_type_param = struct_type_param + `${variableData}, `;
                } else {
                    struct_type_param = struct_type_param + `${variableData}`;
                }
            }
        }
        return struct_type_param;
    }

    MapDisplayValue() {
        let struct_data = ``
        for (let i = 0; i < this._structType.length; i++) {
            if (i > 1) {
                struct_data = struct_data + `cout << "${this._structType[i].value} : " << ptr -> ${this._structType[i].value} << endl;`;
                if (i !== this._structType.length - 1) {
                    struct_data = struct_data + `
            `;
                }
            }
        }
        return struct_data;
    }

    MapSearchParam(searchType) {
        let struct_type_param = ``;
        for (let i = 0; i <  this._structType.length; i++) {
            if ( this._structType[i].value === searchType) {
                struct_type_param = struct_type_param + `${ this._structType[i].type} ${ this._structType[i].value}`;
            }
        }

        return struct_type_param;
    }

    MainSearchParam(index, searchType) {
        let searchParam = ``;
        for (let i = 0; i < this._structType.length; i++) {
            if(this._structType[i].value === searchType) {
                if (this._structType[i].type === "string") {
                    searchParam = `"${index}"`;
                } else if (this._structType[i].type === "int") {
                    searchParam = index;
                } else {
                    searchParam = index;
                }
        
            }
        }
        return searchParam;
    }

    Display() {
        let display = `
    struct ${this._structName} *display(struct ${this._structName} *start)
    {
        struct ${this._structName} *ptr;
        int index = 0;
        ptr = start;
        while(ptr != NULL) {
            cout << "INDEX " << index << endl;
            cout << "DATA " << endl;
            ${this.MapDisplayValue()}
            cout << endl;
            ptr = ptr -> next;
            index++;
        }
        return start;
    }
    `;
    return display;
    }

    AddNode() {
        let add = `
    struct ${this._structName} *add_new_node (struct ${this._structName} *start, ${this.MapParam()})
    {
        ${this._structName} *new_node, *ptr;

        new_node = (struct ${this._structName}*)malloc(sizeof(struct ${this._structName}));
        ${this.MapValue('new_node')}
        ptr = start;

        if (start != NULL) {
            while(ptr -> next != NULL) {
                ptr = ptr -> next;
            }

            new_node -> prev = ptr;
            ptr -> next = new_node;
            new_node -> next = NULL;
        } else {
            new_node -> next = NULL;
            new_node -> prev = NULL;
            start = new_node;
        }
        return start;
    }
    `;
    return add;
    }

    InsertBeforeIndex() {
        let insertInIndex = `
    struct ${this._structName} *insert_before_index(struct ${this._structName} *start, int index, ${this.MapParam()})
    {
        ${this._structName} *new_node, *ptr;

    	new_node = (struct ${this._structName} *)malloc(sizeof(struct ${this._structName}));
    	${this.MapValue('new_node')}
    	
    	if(index < 1) {
    	    new_node -> next = start;
            new_node -> prev = start -> prev;
    	    start = new_node;
    	    
    	} else {
    	    ptr = start;
    	    for (int i = 0; i < index - 1; i++) {
    	        ptr = ptr -> next;
    	    }
    	    
    	    new_node -> next = ptr -> next;
            new_node -> prev = ptr;
    	    ptr -> next = new_node;
            ptr -> next -> prev = new_node;
    	}
    		
    	return start;
    }
    `;
    return insertInIndex;
    }

    InsertAfterIndex() {
        let insertAfterIndex = `
    struct ${this._structName}  *insert_after_index(struct ${this._structName}  *start, int index, ${this.MapParam()})
    {
        ${this._structName}  *new_node, *ptr;

    	new_node = (struct ${this._structName}  *)malloc(sizeof(struct ${this._structName} ));
    	${this.MapValue('new_node')}
    	
    	ptr = start;
    	for (int i = 0; i < index; i++) {
    	    ptr = ptr -> next;
    	}
    	
    	new_node -> next = ptr -> next;
        new_node -> prev = ptr;
        ptr -> next = new_node;
        ptr -> next -> prev = new_node;
	
    	return start;
    }
    `;
    return insertAfterIndex;
    }

    SearchData(searchType) {
        let searchData = `
    struct ${this._structName} *search_data(struct ${this._structName} *start, ${this.MapSearchParam(searchType)})
    {
        ${this._structName} *ptr;
        ptr = start;
        int index = 0;
        while (ptr != NULL) 
        { 
            if (ptr -> ${searchType} == ${searchType}) {
                cout << "${searchType} : " << ${searchType} << endl;
                cout << "Ada di INDEX "<< index << endl << endl;
                return start;
            }
            ptr = ptr->next; 
            index++;
        }
        cout << "TIDAK ADA" << endl;
        return start; 
    }
    `;

    return searchData
    }

    DeleteInIndex() {  
    let deleteInIndex = `
    struct ${this._structName} *delete_in_index(struct ${this._structName} *start, int index)
    {
        ${this._structName} *ptr;
        ptr = start;

        if (index < 1) {
            start = ptr -> next;
            ptr -> next -> prev = NULL;
            ptr -> next = NULL;
            free(ptr);
        } else {
            for(int i = 0; i < index - 1; i++) {
                ptr = ptr -> next;
            }
            ${this._structName} *deleteNode = ptr -> next;
            if (deleteNode -> next == NULL)
                ptr -> next = NULL;
            else {
                ptr -> next = deleteNode -> next;
                ptr -> next -> prev = ptr;
            }
            deleteNode -> next = NULL;
            deleteNode -> prev = NULL;
            free(deleteNode);
        }
        return start;
    }
    `;
    return deleteInIndex;
    }

    MainOperation(operateMainFunction){
        let functionOperation = ``;
        for(let i = 0; i < operateMainFunction.length; i++) {
            functionOperation = functionOperation + `start = ${operateMainFunction[i]}
        `;
        }
        let mainFunction = `
    int main(int argc, char *argv[]) {
        ${functionOperation}
        start = display(start);
        return 0;
    };
        `;
        return mainFunction
    }
    
    GenerateSourceCode(operations, searchType) {
        let operateFunction = ``;
        for(let i = 0; i < operations.length; i++) {
            switch(operations[i]) {
                case 'add':
                    operateFunction = operateFunction + this.AddNode();
                    break;
                case 'before':
                    operateFunction = operateFunction + this.InsertBeforeIndex();
                    break;
                case 'after':
                    operateFunction = operateFunction + this.InsertAfterIndex();
                    break;
                case 'search':
                    operateFunction = operateFunction + this.SearchData(searchType);    
                    break;
                case 'delete':
                    operateFunction = operateFunction + this.DeleteInIndex();
                    break;
                default:
                    console.log("operation not found");
            }
        }
        operateFunction = operateFunction + this.Display();
        let sourceCode = this.head + this.GenerateStruct() + operateFunction;
        return sourceCode;
    }

    OperateMainFunction(operation, structData, index, searchType) {
        let operateMainFunction = ``;
        switch(operation) {
            case 'add':
                operateMainFunction = `add_new_node(start, ${this.MapParamFunc(structData)});`;
                break;
            case 'before':
                operateMainFunction = `insert_before_index(start, ${index}, ${this.MapParamFunc(structData)});`
                break;
            case 'after':
                operateMainFunction = `insert_after_index(start, ${index - 1}, ${this.MapParamFunc(structData)});`
                break;
            case 'search':
                operateMainFunction = `search_data(start, ${this.MainSearchParam(index, searchType)});`
                break;
            case 'delete':
                operateMainFunction = `delete_in_index(start, ${index});`
                break;
            default:
                console.log("operation not found");
        }
        return operateMainFunction;
    }

    GenerateMainFunction(operateMainFunction) {
        let mainFunction = this.MainOperation(operateMainFunction);
        return mainFunction;
    }
}