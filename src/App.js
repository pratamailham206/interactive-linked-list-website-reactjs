import React from 'react';
import { Route, BrowserRouter as Router, Switch } from 'react-router-dom';
import Login from './views/auth/login';
import Register from './views/auth/register';
import Home from './views/home/home';
import Struct from './views/struct/struct';
import Navbar from './views/templates/navbar';
import { ProvideAuth } from './authentication/auth';
import DashboardView from './views/dashboard/dashboard';
import Editor from './views/editor/editor';

function App() {
  return (
    <ProvideAuth>
        <Router>
          <Navbar />
          <Switch>
            <Route exact path="/" component={Home} />
            <Route path="/register" component={Register} />
            <Route path="/login" component={Login} />
            <Route path="/dashboard" component={DashboardView} />
            <Route path="/struct/:type/:listid" component={Struct} />
            <Route path="/editor/:listid" component={Editor} />
          </Switch>
        </Router>
    </ProvideAuth>
  );
}

export default App;
