<h1 align="center">Interactive Linked List</h1>

[![React](https://img.shields.io/badge/React-20232A?style=for-the-badge&logo=react&logoColor=61DAFB)](https://reactjs.org/)
[![Redux](https://img.shields.io/badge/Redux-593D88?style=for-the-badge&logo=redux&logoColor=white)](https://redux.js.org/)
[![Typescript](https://img.shields.io/badge/TypeScript-007ACC?style=for-the-badge&logo=typescript&logoColor=white)](https://www.typescriptlang.org/)

Web-based learning media, with interesting interactions where users can explore by creating linked-list, adding, searching, deleting node through the tools contained in the application. After the user performs the operation on the linked list, the source code will generate as a reference for how the user can implement the linked list that they created in their respective programs.

<img src="public/static/images/1.PNG"  width="33%">
<img src="public/static/images/6.PNG"  width="33%">
<img src="public/static/images/7.PNG"  width="33%">

#### Features

- Authentication User: Login, register, send validation with custom API
- Visualization of linked list
- Create new Linked List
- Create new struct
- Saved Linked List
- Quest system
- All node operation (create, add, search, delete)
- Generate Source Code based on Linked List created

#### Platform

Web

### Project status

This project is still in development, see [develop](https://github.com/IlhamPratama1/InteractiveLinkedListApp-Website-ReactJs)

# Getting started

Install

### `npm run start`

Start project.

### `npm run start`

Build

### `npm run build`

# What's included

- Introduction Page
- Authentication User
- CRUD data integrated with custom API
- Saved Linked List project
- Validation
- Unit tests including code coverage
- Quest system
- Guideline

### Redux

State management using React Redux

### Custom API

this project integrated with [Custom API](https://github.com/IlhamPratama1/InteractiveLinkedList-API-NodeJs) using Express Node Js 

# Authors

* Ilham Pratama - [Gitlab](https://gitlab.com/pratamailham206)

# Screenshot

<img src="public/static/images/1.PNG"  width="33%">
<img src="public/static/images/2.PNG"  width="33%">
<img src="public/static/images/3.PNG"  width="33%">
<img src="public/static/images/4.PNG"  width="33%">
<img src="public/static/images/5.PNG"  width="33%">
<img src="public/static/images/6.PNG"  width="33%">
<img src="public/static/images/7.PNG"  width="33%">

# License

This project is licensed under the MIT License - see the [LICENSE](LICENSE) file for details
